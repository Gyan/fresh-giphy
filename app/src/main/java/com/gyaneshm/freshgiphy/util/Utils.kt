package com.gyaneshm.freshgiphy.util

import java.util.UUID

/**
 * Created by Gyanesh Mishra on 9/21/2017.

 * Contains commonly used methods or one-off methods

 */

class Utils{
    private val TAG = Utils::class.java.name

    /***
     * Generate a UUID for a given string
     * UUIDs are more reliable for something like URLs as opposed to hashes
     *
     * We use it here to ensure unique filenames for each gif that is downloaded
     */
    fun generateUUID(string: String): String {
        return UUID.nameUUIDFromBytes(string.toByteArray()).toString()
    }

}
