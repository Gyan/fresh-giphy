package com.gyaneshm.freshgiphy.ui.activities

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter

import com.gyaneshm.freshgiphy.R
import com.gyaneshm.freshgiphy.ui.fragments.favorite.FavoriteFragment
import com.gyaneshm.freshgiphy.ui.fragments.search.SearchFragment

/**
 * Created by Gyanesh Mishra on 9/19/2017
 *
 * Simple adaptor for the viewpager used by the MainActivity
 * Adds 2 tabs: search and favorite
 * and allows you to swipe between them
 */

class MainActivityPagerAdapter(private val mContext: Context, fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val TAG = MainActivityPagerAdapter::class.java.name

    override fun getItem(position: Int): Fragment {
        if (position == 0) {
            return SearchFragment()
        } else if (position == 1) {
            return FavoriteFragment()
        } else {
            return SearchFragment()
        }
    }

    override fun getCount(): Int {
        return 2
    }

    // This determines the title for each tab
    override fun getPageTitle(position: Int): CharSequence? {
        // Generate title based on item position
        when (position) {
            0 -> return mContext.getString(R.string.main_activity_tab_search)
            1 -> return mContext.getString(R.string.main_activity_tab_favorites)
            else -> return null
        }
    }
}
