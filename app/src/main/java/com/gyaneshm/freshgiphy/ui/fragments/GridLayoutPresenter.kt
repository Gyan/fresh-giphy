package com.gyaneshm.freshgiphy.ui.fragments

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.gyaneshm.freshgiphy.R
import com.gyaneshm.freshgiphy.data.ExtendedGif
import com.gyaneshm.freshgiphy.data.StoreGifToStorage
import com.gyaneshm.freshgiphy.data.StoredGif
import com.gyaneshm.freshgiphy.ui.RxEventBus
import io.reactivex.Observer
import io.reactivex.disposables.Disposable

/**
 * Created by Gyanesh Mishra on 9/24/2017.
 * Presenter class to interface between StoredGif/StoreGifToStorage Model
 * and the gridlayout view
 */
class GridLayoutPresenter(private val context: Context) {

    private val TAG = GridLayoutPresenter::class.java.name

    /***
     * Deletes a gif from the local storage and displays the relevant toast
     * and fires off an event bus message for views to get updated
     */
    fun deleteGif(gif: ExtendedGif) {
        StoredGif(context).deleteByFilename(gif.storedFileName!!)
        Toast.makeText(context, R.string.toast_message_gif_removed , Toast.LENGTH_SHORT).show()
        RxEventBus.publish(RxEventBus.ValidActions.ACTION_REMOVE_GIF, gif)
    }

    /***
     * Saves a gif to the local storage and displays the relevant toast
     * and fires off an event bus message for views to get updated
     *
     */
    fun saveGif(gif: ExtendedGif) {

        // Set a observer to notify user of file download success/failure
        val observer = object : Observer<Any>{
            override fun onNext(t: Any) {
                Toast.makeText(context, R.string.toast_downloading_message_success , Toast.LENGTH_SHORT).show()
                RxEventBus.publish(RxEventBus.ValidActions.ACTION_STORE_GIF, R.string.eventbus_gif_saved_message)
            }

            override fun onError(e: Throwable) {
                Toast.makeText(context, R.string.toast_downloading_message_failure , Toast.LENGTH_SHORT).show()
                Log.e(TAG, "Failed to download gif because: "+ e.toString())
            }

            override fun onComplete() {
            }

            override fun onSubscribe(d: Disposable) {
            }
        }

        // Download the gif that's been favorite
        val downloadGif = StoreGifToStorage(context, gif.gifUrl, observer)
        downloadGif.execute()

    }
}