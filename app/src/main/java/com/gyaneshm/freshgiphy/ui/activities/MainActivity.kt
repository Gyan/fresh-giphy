package com.gyaneshm.freshgiphy.ui.activities

import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View

import com.gyaneshm.freshgiphy.R

/***
 * Main Activity that loads on app startup
 * Displays the 2 fragments using a viewpager under a tablayout
 */

class MainActivity : AppCompatActivity() {

    private val TAG = MainActivity::class.java.name

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Find the view pager that will allow the user to swipe between fragments
        val viewPager = findViewById<View>(R.id.main_activity_pagerview) as ViewPager

        // Create an adapter that knows which fragment should be shown on each page
        val adapter = MainActivityPagerAdapter(this, supportFragmentManager)

        // Set the adapter onto the view pager
        viewPager.adapter = adapter

        // Give the TabLayout the ViewPager
        val tabLayout = findViewById<View>(R.id.main_activity_tabs) as TabLayout
        tabLayout.setupWithViewPager(viewPager)
    }
}
