package com.gyaneshm.freshgiphy.ui.fragments.search

import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView


/**
 * Created by Gyanesh Mishra on 9/22/2017.
 * View class that implements infinite scrolling for the a recyclerView
 *
 */
abstract class EndlessRecyclerViewListener(private var gridLayoutManager: GridLayoutManager) : RecyclerView.OnScrollListener() {
    private var visibleThreshold = 10
    private var currentPage = 0
    private var previousTotalItemCount = 0
    private var loading = true
    private var startingPageIndex = 0

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        // Clear out the recyclerView caching pool to avoid using a view with favorite image
        // for a view with non-favorite image
        recyclerView.recycledViewPool.clear()

        // Only load data when scrolled up (scrolling direction down)
        if (dy > 0) {
            val lastVisibleItemPosition = gridLayoutManager.findLastVisibleItemPosition()
            val totalItemCount = gridLayoutManager.itemCount

            // If it’s still loading, we check to see if the dataset count has
            // changed, if so we conclude it has finished loading and update the current page
            // number and total item count.
            if (loading && (totalItemCount > previousTotalItemCount)) {
                loading = false
                previousTotalItemCount = totalItemCount
            }

            // If it isn’t currently loading, we check to see if we have breached
            // the visibleThreshold and need to reload more data.
            // If we do need to reload some more data, we execute onLoadMore to fetch the data.
            // threshold should reflect how many total columns there are too
            if (!loading && lastVisibleItemPosition + visibleThreshold >= totalItemCount) {
                currentPage++
                onLoadMore(currentPage, totalItemCount, recyclerView)
                loading = true
            }
        }
    }

    // Call this method whenever performing new searches
    fun resetState() {
        this.currentPage = this.startingPageIndex
        this.previousTotalItemCount = 0
        this.loading = true
    }

    // Defines the process for actually loading more data based on page
    abstract fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView)

}