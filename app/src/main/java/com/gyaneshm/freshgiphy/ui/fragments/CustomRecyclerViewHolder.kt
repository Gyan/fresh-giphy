package com.gyaneshm.freshgiphy.ui.fragments

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.gif.GifDrawable
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.gyaneshm.freshgiphy.R
import com.gyaneshm.freshgiphy.data.ExtendedGif

/**
 * Created by Gyanesh Mishra on 9/24/2017.
 *
 * Custom ViewHolder for the recyclerView
 * Contains the gifview, a progressbar and the favoritebutton
 * Uses glide library to load gifs into the imageview
 *
 */
class CustomRecyclerViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    private var favoriteButton: ImageButton = itemView.findViewById<View>(R.id.giffavoriteimgbtn) as ImageButton
    private var gifImageView: ImageView = itemView.findViewById<View>(R.id.gif_image_view) as ImageView
    private var progressBar: ProgressBar = itemView.findViewById<View>(R.id.gif_loading_progress_bar) as ProgressBar
    private lateinit var glideProgressListener: RequestListener<GifDrawable>
    private lateinit var presenter: GridLayoutPresenter
    private lateinit var context: Context

    fun bind(gif: ExtendedGif) {
        context = itemView.context
        presenter = GridLayoutPresenter(context)

        // Define the listener for glide to make the progress bar disapperar after the gif is done loading
        // Also add a nice black background for cinematic effect
        glideProgressListener = object : RequestListener<GifDrawable> {
            override fun onLoadFailed(e: GlideException?, model: Any?, target: Target<GifDrawable>?, isFirstResource: Boolean): Boolean {
                progressBar.visibility = View.GONE
                gifImageView.setBackgroundColor(Color.BLACK)
                return false
            }

            override fun onResourceReady(resource: GifDrawable?, model: Any?, target: Target<GifDrawable>?, dataSource: DataSource?, isFirstResource: Boolean): Boolean {
                progressBar.visibility = View.GONE
                gifImageView.setBackgroundColor(Color.BLACK)
                return false
            }
        }

        // Load the Gif
        Glide.with(context).asGif().load(gif.gifUrl).listener(glideProgressListener).into(gifImageView)

        // If the gif is saved locally, set the favorite button to red
        if (gif.isFavorite) {
            favoriteButton.setImageResource(R.drawable.ic_favorite_color_48dp)
        }

        // Handle the clicking of favorite button
        favoriteButton.setOnClickListener {
            // If it's already downloaded, remove it and update the button
            if (gif.isFavorite) {
                presenter.deleteGif(gif)
                gif.isFavorite = false
                favoriteButton.setImageResource(R.drawable.ic_favorite_border_white_48dp)
                // If it's not, then download it and update the favorite button
            } else {
                presenter.saveGif(gif)
                gif.isFavorite = true
                favoriteButton.setImageResource(R.drawable.ic_favorite_color_48dp)
            }
        }
    }
}