package com.gyaneshm.freshgiphy.ui.fragments.favorite

import android.content.Context
import com.gyaneshm.freshgiphy.data.ExtendedGif
import com.gyaneshm.freshgiphy.data.StoredGif

/**
 * Created by Gyanesh Mishra on 9/24/2017.
 * Presenter class to interact with the Storage Model
 * and return data to the view
 *
 */
class FavoriteFragmentPresenter(var context: Context) {

    fun getAllGifs(): ArrayList<ExtendedGif>{
        return StoredGif(context).getAll()
    }
}