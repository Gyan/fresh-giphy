package com.gyaneshm.freshgiphy.ui

import io.reactivex.annotations.NonNull
import io.reactivex.subjects.PublishSubject
import io.reactivex.functions.Consumer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.android.schedulers.AndroidSchedulers
import android.util.SparseArray

/**
 * Created by Gyanesh Mishra on 9/23/2017.
 * EventBus that can be used to send messages across
 * A little modified solution from the Pierce Zaifman Example
 * https://piercezaifman.com/how-to-make-an-event-bus-with-rxjava-and-rxandroid/
 */
object RxEventBus {

    private val TAG = RxEventBus::class.java.name

    // Map of all the subjects (observable)
    private val sSubjectMap = SparseArray<PublishSubject<Any>>()

    // Map of all the subscriptions (observers/listeners)
    private val sSubscriptionsMap = HashMap<Any, CompositeDisposable>()

    enum class ValidActions(val code: Int) {
        ACTION_STORE_GIF(0),
        ACTION_REMOVE_GIF(1)
    }

    /**
     * Get the subject or create it if it's not already in memory.
     */
    @NonNull
    private fun getSubject(subjectCode: ValidActions): PublishSubject<Any> {
        var subject: PublishSubject<Any>? = sSubjectMap.get(subjectCode.code)
        if (subject == null) {
            subject = PublishSubject.create()
            subject!!.subscribeOn(AndroidSchedulers.mainThread())
            sSubjectMap.put(subjectCode.code, subject)
        }

        return subject
    }

    /**
     * Get the CompositeDisposable or create it if it's not already in memory.
     */
    @NonNull
    private fun getCompositeDisposable(@NonNull inObject: Any): CompositeDisposable {
        var compositeDisposable: CompositeDisposable? = sSubscriptionsMap.get(inObject)
        if (compositeDisposable == null) {
            compositeDisposable = CompositeDisposable()
            sSubscriptionsMap.put(inObject, compositeDisposable)
        }

        return compositeDisposable
    }

    /**
     * Subscribe to the specified subject and listen for updates on that subject. Pass in an object to associate
     * your registration with, so that you can unsubscribe later.
     * **Note:** Make sure to call [RxBus.unregister] to avoid memory leaks.
     */
    fun subscribe(subject: ValidActions, @NonNull lifecycle: Any, @NonNull action: Consumer<Any>) {
        val disposable = getSubject(subject).subscribe(action)
        getCompositeDisposable(lifecycle).add(disposable)
    }

    /**
     * Unregisters this object from the bus, removing all subscriptions.
     * This should be called when the object is going to go out of memory.
     */
    fun unregister(@NonNull lifecycle: Any) {
        //We have to remove the composition from the map, because once you dispose it can't be used anymore
        val compositeDisposable = sSubscriptionsMap.remove(lifecycle)
        if (compositeDisposable != null) {
            compositeDisposable.dispose()
        }
    }

    /**
     * Publish an object to the specified subject for all subscribers of that subject.
     */
    fun publish(subject: ValidActions, @NonNull message: Any) {
        getSubject(subject).onNext(message)
    }
}