package com.gyaneshm.freshgiphy.ui.fragments

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup

import com.gyaneshm.freshgiphy.data.ExtendedGif
import com.gyaneshm.freshgiphy.R

/**
 * Created by Gyanesh Mishra on 9/19/2017.
 * Adaptor for the GridLayout on the fragments.
 *
 * Takes in a list of ExtendedGifs and populates the view
 */

class GridLayoutAdaptor(private val gifs: ArrayList<ExtendedGif>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TAG = GridLayoutAdaptor::class.java.name
    private lateinit var viewHolder: CustomRecyclerViewHolder

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CustomRecyclerViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.gridlayout_row, parent, false)
        val viewHolder = CustomRecyclerViewHolder(v)
        return viewHolder
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        viewHolder = holder as CustomRecyclerViewHolder
        viewHolder.bind(gifs[position])
    }

    fun setGifs(newGifs: ArrayList<ExtendedGif>) {
        gifs.clear()
        gifs.addAll(newGifs)

        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return gifs.size
    }

}
