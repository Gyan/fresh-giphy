package com.gyaneshm.freshgiphy.ui.fragments.search


import android.app.Activity
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.TextView

import com.gyaneshm.freshgiphy.data.ExtendedGif
import com.gyaneshm.freshgiphy.R

import java.util.ArrayList

import `in`.myinnos.gifimages.builder.GiphyQueryBuilder
import com.gyaneshm.freshgiphy.ui.RxEventBus
import com.gyaneshm.freshgiphy.ui.fragments.GridLayoutAdaptor
import io.reactivex.functions.Consumer

/**
 * Search Fragment Loads all the searched Gifs.
 * By default it displays the Trending views
 */
class SearchFragment : Fragment() {

    private val TAG = SearchFragment::class.java.name
    private lateinit var recyclerView: RecyclerView
    private lateinit var editText: EditText
    private lateinit var presenterCallback: SearchFragmentPresenter.Callback
    private lateinit var presenter: SearchFragmentPresenter
    private lateinit var gridLayoutAdaptor: GridLayoutAdaptor
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var recyclerViewOnScrollListener: EndlessRecyclerViewListener
    private var currentGifEndpoint: SearchFragmentPresenter.Endpoint = SearchFragmentPresenter.Endpoint.TRENDING
    private var currentSearchText: String = ""


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_search, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        RxEventBus.unregister(lifecycle)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        // Instantiate all variables
        recyclerView = view!!.findViewById<View>(R.id.recyclerView) as RecyclerView
        editText = view!!.findViewById<View>(R.id.search_gif_input) as EditText
        gridLayoutManager = GridLayoutManager(context, 2)
        gridLayoutAdaptor = GridLayoutAdaptor(ArrayList())
        presenterCallback = object: SearchFragmentPresenter.Callback {
            override fun onResponse(gifs: ArrayList<ExtendedGif>) {
                gridLayoutAdaptor.setGifs(gifs)
            }
        }
        presenter = SearchFragmentPresenter(context, presenterCallback)

        // Set the layoutmanager and adaptor
        recyclerView.layoutManager = gridLayoutManager
        recyclerView.adapter = gridLayoutAdaptor

        // Setup On click listener for scrolling
        recyclerViewOnScrollListener = object : EndlessRecyclerViewListener(gridLayoutManager) {
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                if (currentGifEndpoint.equals(GiphyQueryBuilder.EndPoint.SEARCH)) {
                    presenter.getBySearchTerm(page = page, searchText = currentSearchText)
                }else {
                    presenter.getTrending(page = page)
                }
            }
        }
        recyclerView.addOnScrollListener(recyclerViewOnScrollListener)

        // Load the trending gifs by default
        presenter.getTrending()

        // If a gif is removed from  the Favorite fragment, the Search Fragment view still has the favorite set
        // Update the list and refresh the view
        RxEventBus.subscribe(RxEventBus.ValidActions.ACTION_REMOVE_GIF, lifecycle, Consumer { t ->
            var removedGif = t as ExtendedGif
            gridLayoutAdaptor.setGifs(presenter.updateListAfterGifRemoved(removedGif))
        })

        // Set the listener for search on EditText
        editText.setOnEditorActionListener(TextView.OnEditorActionListener { view, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH || event.keyCode == KeyEvent.KEYCODE_ENTER) {

                // Hide the keyboard after user hits search
                val inputMethodManager = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
                inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)

                var searchText = editText.text.toString()

                // Do cleanup
                presenter.clearList()
                recyclerView.scrollToPosition(0)
                recyclerViewOnScrollListener.resetState()

                // If it's empty, go back to trending, trim any whitespaces
                if (searchText.trim().isEmpty()) {
                    editText.setText("")
                    currentSearchText = ""
                    currentGifEndpoint = SearchFragmentPresenter.Endpoint.TRENDING
                    presenter.getTrending()
                }
                // Show the search results if it's a search
                else {
                    currentSearchText = searchText
                    currentGifEndpoint = SearchFragmentPresenter.Endpoint.SEARCH
                    presenter.getBySearchTerm(searchText = currentSearchText)
                }

                return@OnEditorActionListener true
            }
            return@OnEditorActionListener false
        })

    }
}
