package com.gyaneshm.freshgiphy.ui.fragments.search

import `in`.myinnos.gifimages.GiphyTask
import `in`.myinnos.gifimages.builder.GiphyQueryBuilder
import `in`.myinnos.gifimages.model.Gif
import android.content.Context
import com.gyaneshm.freshgiphy.data.ExtendedGif
import com.gyaneshm.freshgiphy.data.StoredGif
import com.gyaneshm.freshgiphy.util.Utils

/**
 * Created by Gyanesh Mishra on 9/24/2017.
 * Presenter class that interacts directly with the Gif Model
 * provided by the GiphyImageResultsPreview library.
 *
 * Inherits some properties from the giphy API task so that the only
 * class that the view interacts is this one.
 *
 * When we decide to change libraries or add more gif sources, we'd only need
 * to modify this class :)
 *
 */
class SearchFragmentPresenter(var context: Context, var fragmentCallback: Callback) {

    private val GIFS_PER_PAGE: Int = 10
    private var presenterCallback: GiphyTask.Callback
    private var gifList = ArrayList<ExtendedGif>()

    enum class Endpoint (var endpoint: String) {
        TRENDING("trending"),
        SEARCH("search")
    }

    interface Callback {
        fun onResponse(gifs: ArrayList<ExtendedGif>)
    }

    init {
        presenterCallback = GiphyTask.Callback { gifs ->
            for (gif in gifs) {
                gifList.add(convertGifToExtendedGif(gif))
            }
            fragmentCallback.onResponse(gifList)
        }
    }


    fun getTrending(page: Int = 0) {
        val builder = GiphyQueryBuilder(GiphyQueryBuilder.EndPoint.TRENDS, "")
                .setLimit(GIFS_PER_PAGE)
                .setOffset(GIFS_PER_PAGE*page)

        GiphyTask(builder.build(), presenterCallback).execute()
    }

    fun getBySearchTerm(page: Int = 0, searchText: String) {
        val builder = GiphyQueryBuilder(GiphyQueryBuilder.EndPoint.SEARCH, "")
                .setQuery(searchText)
                .setLimit(GIFS_PER_PAGE)
                .setOffset(GIFS_PER_PAGE*page)

        GiphyTask(builder.build(), presenterCallback).execute()
    }

    fun clearList() {
        // Clear the list and notify the adapter of the change
        gifList.clear()
        fragmentCallback.onResponse(gifList)
    }

    fun updateListAfterGifRemoved(gif: ExtendedGif): ArrayList<ExtendedGif> {
        for (i in gifList.indices) {
            val currentGif = gifList[i]
            // Format gifUrl into its downloaded name
            val currentGifStoredName = Utils().generateUUID(currentGif.gifUrl) + ".gif"
            if ((currentGifStoredName == gif.storedFileName && (currentGif.isFavorite))) {
                val updatedGif = ExtendedGif(currentGif.previewImageUrl, currentGif.previewMp4Url, currentGif.gifUrl, false, currentGif.storedFileName!!)
                gifList[i] = updatedGif
                return gifList
            }
        }

        return ArrayList()
    }

    /***
     * Helper method to convert Gif model to ExtendedGif model
     */
    private fun convertGifToExtendedGif(gif: Gif): ExtendedGif {
        val gifUrl = gif.gifUrl
        // Format gifUrl into its downloaded name
        val gifStoredName = Utils().generateUUID(gifUrl) + ".gif"

        // Check if it's stored already
        val isFavorite =  StoredGif(context).getAllFileNames().contains(gifStoredName)
        return ExtendedGif(gif, isFavorite, gifStoredName)
    }

}