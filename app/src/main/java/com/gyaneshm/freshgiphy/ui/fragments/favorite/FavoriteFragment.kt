package com.gyaneshm.freshgiphy.ui.fragments.favorite

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.gyaneshm.freshgiphy.R
import com.gyaneshm.freshgiphy.data.StoredGif
import com.gyaneshm.freshgiphy.ui.RxEventBus
import com.gyaneshm.freshgiphy.ui.fragments.GridLayoutAdaptor
import io.reactivex.functions.Consumer

import java.util.ArrayList

/**
 * View class that displays all the locally stored gifs
 */

class FavoriteFragment : Fragment() {

    private val TAG = FavoriteFragment::class.java.name
    private lateinit var recyclerView: RecyclerView
    private lateinit var gridLayoutAdaptor: GridLayoutAdaptor
    private lateinit var gridLayoutManager: GridLayoutManager
    private lateinit var presenter: FavoriteFragmentPresenter

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater!!.inflate(R.layout.recyclerview_layout, container, false)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        RxEventBus.unregister(lifecycle)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        // Instantiate all variables
        recyclerView = view!!.findViewById<View>(R.id.recyclerView) as RecyclerView
        gridLayoutManager = GridLayoutManager(context, 2)
        gridLayoutAdaptor = GridLayoutAdaptor(ArrayList())
        presenter = FavoriteFragmentPresenter(context)

        // Set the above as the layoutmanager for our recyclerview
        recyclerView.layoutManager = gridLayoutManager

        // set the Adapter to RecyclerView
        recyclerView.adapter = gridLayoutAdaptor

        // Load the gifs stored on the local disk
        gridLayoutAdaptor.setGifs(presenter.getAllGifs())

        // Update the fragment with the latest GifList when a gif is downloaded/removed
        RxEventBus.subscribe(RxEventBus.ValidActions.ACTION_STORE_GIF, lifecycle, Consumer { gridLayoutAdaptor.setGifs(presenter.getAllGifs()) })
        RxEventBus.subscribe(RxEventBus.ValidActions.ACTION_REMOVE_GIF, lifecycle, Consumer { gridLayoutAdaptor.setGifs(presenter.getAllGifs()) })
    }

}
