package com.gyaneshm.freshgiphy.data

import android.content.Context
import android.os.AsyncTask
import android.util.Log
import com.gyaneshm.freshgiphy.util.Utils

import com.gyaneshm.freshgiphy.R
import io.reactivex.Observable
import io.reactivex.Observer

import java.io.File
import java.io.FileOutputStream
import java.net.HttpURLConnection
import java.net.URL

/**
 * Created by Gyanesh Mishra on 9/20/2017.

 * Model class that downloads a gif to the device's internal storage.
 * Sets the filename by combining UUID of the URL + .gif

 * UUID of a URL is important to ensure uniquness
 * and it allows us to check the if a gif has already been downloaded or not
 */

class StoreGifToStorage(private val appContext: Context, private val gifUrl: String, private val observer: Observer<Any>? = null) : AsyncTask<Void, Void, File>() {
    private val TAG = StoreGifToStorage::class.java.name

    companion object {
        private val BUFFER_SIZE = 4096
    }

    override fun doInBackground(vararg voids: Void): File? {
        try {
            val url = URL(gifUrl)
            val httpConn = url.openConnection() as HttpURLConnection
            val responseCode = httpConn.responseCode
            if (responseCode == HttpURLConnection.HTTP_OK) {
                val file = File(appContext.filesDir, Utils().generateUUID(gifUrl) + ".gif")

                // If the gif already exists, skip
                if (file.exists()) {
                    return file
                }

                file.createNewFile()

                // opens input stream from the HTTP connection
                val inputStream = httpConn.inputStream

                // opens an output stream to save into file
                val outputStream = FileOutputStream(file)

                val buffer = ByteArray(BUFFER_SIZE)
                var done = false
                while (!done) {
                    val bytesRead = inputStream.read(buffer)
                    if (bytesRead == -1) {
                        done = true
                        continue
                    }
                    outputStream.write(buffer, 0, bytesRead)
                }

                outputStream.close()
                inputStream.close()

                return file
            } else {
                Log.v(TAG, "No file to download. Server replied HTTP code: " + responseCode)
            }

            httpConn.disconnect()
        } catch (e: Exception) {
            e.printStackTrace()
        }

        return null
    }

    override fun onPostExecute(result: File?) {
        if (result != null && observer!= null) {
            // Send the success message back to the observer if there is one
            val observable = Observable.just(R.string.rx_observable_gif_saved_message)
            observable.subscribe(observer)
        }
    }

}
