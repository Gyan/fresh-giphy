package com.gyaneshm.freshgiphy.data

import `in`.myinnos.gifimages.model.Gif

/**
 * Created by Gyanesh Mishra on 9/21/2017.

 * Extended gif adds on a couple functionality to the Gif model in
 * GiphyResultsPreview Library.

 * - Adds isfavorite attribute: If a gif is favorite, its stored locally on the device
 * - Adds a field for the stored filename so that we can match it against the downloaded gifs
 */

class ExtendedGif : Gif {

    var isFavorite: Boolean = false
    var storedFileName: String? = null

    constructor(previewImageUrl: String, previewMp4Url: String, gifUrl: String, isFavorite: Boolean,  storedFileName: String) : super(previewImageUrl, previewMp4Url, gifUrl) {
        this.isFavorite = isFavorite
        this.storedFileName = storedFileName
    }

    // Special constructor that takes in a gif object and extra params and returns an ExtenedGif
    constructor(gif: Gif, isFavorite: Boolean,  storedFileName: String) : super(gif.previewImageUrl, gif.previewMp4Url, gif.gifUrl) {
        this.isFavorite = isFavorite
        this.storedFileName = storedFileName
    }

}
