package com.gyaneshm.freshgiphy.data

import android.content.Context
import android.net.Uri
import java.util.ArrayList

/**
 * Created by Gyanesh Mishra on 9/24/2017.
 * Model class to interact with all the gifs stored on the local storage
 */
class StoredGif (private val context: Context) {
    /***
     * Queries the storage and returns all the stored gifs on the disk

     * @return List of ExtendedGifs
     */
    fun getAll(): ArrayList<ExtendedGif> {
        val filesDir = context.filesDir
        val files = filesDir.listFiles()

        val gifs = ArrayList<ExtendedGif>()

        for (file in files) {
            gifs.add(ExtendedGif(Uri.fromFile(file).toString(), Uri.fromFile(file).toString(), Uri.fromFile(file).toString(), true, file.name))
        }

        return gifs
    }

    /***
     * Queries the storage and returns just the names of all stored gifs

     * @return List of Strings
     */
    fun getAllFileNames(): List<String> {
        val filesDir = context.filesDir
        val files = filesDir.listFiles()

        val fileNames = ArrayList<String>()

        for (file in files) {
            fileNames.add(file.name)
        }

        return fileNames
    }

    /***
     * Deletes a file from the disk by its filename
     * @param fileName: name of the file to be deleted
     */
    fun deleteByFilename(fileName: String) {
        context.deleteFile(fileName)
    }
}